package com.obivan86.springboot.orm.jpa;


public interface UniqueIdGenerator<T> {
    T getNextUniqueId();
}