package com.obivan86.springboot.user;

import com.obivan86.springboot.orm.jpa.AbstractEntityId;

import java.util.UUID;

public class UserId extends AbstractEntityId<UUID> {

    protected UserId() { //<1>

    }

    public UserId(UUID id) { //<2>
        super(id);
    }
}