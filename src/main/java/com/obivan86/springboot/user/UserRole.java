package com.obivan86.springboot.user;

public enum  UserRole {
    OFFICER,
    CAPTAIN,
    ADMIN
}
